<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');
Auth::routes();

Route::get('/user/{user}','UserController@view')->name('user.view');
Route::get('/user/{user}/edit','UserController@edit')->name('user.edit');
Route::post('/user/{u}/edit','UserController@update')->name('user.update');
Route::post('/user/{u}/edit/j','UserController@update_judge')->name('user.update.judge');
Route::get('/user/{user}/dogs','UserController@view_dogs')->name('user.view.dogs');
Route::get('/user/{user}/clubs','UserController@view_clubs')->name('user.view.clubs');

Route::get('/dog/new','DogController@create')->name('dog.create');
Route::post('/dog/new','DogController@store')->name('dog.store');
Route::get('/dog/publish/{dog}','DogController@publish')->name('dog.publish');
Route::get('/dog/pause/{dog}','DogController@pause')->name('dog.pause');
Route::get('/dog/edit/{dog}','DogController@edit')->name('dog.edit');
Route::post('/dog/edit/{dog}','DogController@update')->name('dog.update');
Route::get('/dog/view/{dog}','DogController@view')->name('dog.view');

Route::get('/club/create', 'ClubController@create')->name('club.create');
Route::post('/club/create', 'ClubController@store')->name('club.store');
Route::get('/club/show/{club}', 'ClubController@show')->name('club.show');
Route::get('/club/dashboard/{club}', 'ClubController@dashboard')->name('club.dashboard');
Route::get('/club/events/{club}', 'ClubController@events')->name('club.events');
Route::get('/club/about/{club}', 'ClubController@about')->name('club.about');
Route::get('/club/administration/{club}', 'ClubController@administration')->name('club.administration');
Route::get('/club/activities/{club}', 'ClubController@activities')->name('club.activities');
Route::get('/club/publish/{club}', 'ClubController@publish')->name('club.publish');
Route::get('/club/edit/{club}', 'ClubController@edit')->name('club.edit');
Route::post('/club/edit/{club}', 'ClubController@update')->name('club.update');

Route::get('/club/administration/add/{club}', 'AdministrationController@create')->name('administration.add');
Route::post('/club/administration/add/{club}', 'AdministrationController@store')->name('administration.store');

Route::get('/club/events/create/{club}', 'EventController@create')->name('event.create');
Route::post('/club/events/create/{club}', 'EventController@store')->name('event.store');
Route::get('/events/show/{event}', 'EventController@show')->name('event.show');
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
