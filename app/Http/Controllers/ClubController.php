<?php

namespace App\Http\Controllers;

use App\Club;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ClubController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('club.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        $rules = array(
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'club_number' => 'required|string|max:255',
            'zip_code' => 'required|string|max:255',
            'email' => 'required|email',

        );
        $validator = \Validator::make($request->all(), $rules);

        // process the validation
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $club = new Club();

            $club->name             = $request->get('name');
            $club->club_number      = $request->get('club_number');
            $club->email            = $request->get('email');
            $club->website          = $request->get('website');
            $club->phone            = $request->get('phone');
            $club->address          = $request->get('address');
            $club->city             = $request->get('city');
            $club->state_id_fk      = $request->get('state_id_fk');
            $club->zip_code         = $request->get('zip_code');
            $club->akc_club_name    = $request->get('akc_club_name');
            $club->akc_club_number  = $request->get('akc_club_number');

            $club->user_id          = Auth::user()->id;
            $club->save();

            \Session::flash('status', 'Your club has been created successfully!');
            \Session::flash('alert-class', 'alert-success');
            return \Redirect::route('club.show',$club);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function show(Club $club)
    {
        if ($club->is_owner(Auth::user()))
        {
            return \Redirect::route('club.dashboard',$club);
        }
        else
        {
            return \Redirect::route('club.events',$club);
        }
    }

    public function dashboard(Club $club)
    {
        if ($club->is_owner(Auth::user()))
        {
            $tab = 'dashboard';
            return view('club.dashboard', compact('club','tab'));
        }else{
            return \Redirect::route('club.events',$club);
        }
    }

    public function events(Club $club)
    {
        $tab = 'events';
        return view('club.events', compact('club','tab'));
    }

    public function about(Club $club)
    {
        $tab = 'about';
        return view('club.about', compact('club','tab'));
    }

    public function activities(Club $club)
    {
        $tab = 'activities';
        return view('club.activities', compact('club','tab'));
    }


    public function publish(Club $club)
    {
        if ($club->is_owner(Auth::user()))
        {
            $club->is_publish = true;
            $club->save();
            return back();
        }else{
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function edit(Club $club)
    {
        if ($club->is_owner(Auth::user()))
        {
            return view('club.edit', compact('club'));
        }
        else
        {
            \Session::flash('status', 'You do not have the permission to access this section!');
            \Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }

    public function administration(Club $club)
    {
        if ($club->is_owner(Auth::user()))
        {
            $tab = 'administration';
            return view('club.administration', compact('club','tab'));
        }
        else
        {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Club $club)
    {

        if ($club->is_owner(Auth::user())){
            // validate
            $rules = array(
                'name' => 'required|string|max:255',
                'phone' => 'required|string|max:255',
                'city' => 'required|string|max:255',
                'address' => 'required|string|max:255',
                'club_number' => 'required|string|max:255',
                'zip_code' => 'required|string|max:255',
                'email' => 'required|email',

            );
            $validator = \Validator::make($request->all(), $rules);

            // process the validation
            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }else{
                $club->name             = $request->get('name');
                $club->club_number      = $request->get('club_number');
                $club->email            = $request->get('email');
                $club->website          = $request->get('website');
                $club->phone            = $request->get('phone');
                $club->address          = $request->get('address');
                $club->city             = $request->get('city');
                $club->state_id_fk      = $request->get('state_id_fk');
                $club->zip_code         = $request->get('zip_code');
                $club->akc_club_name    = $request->get('akc_club_name');
                $club->akc_club_number  = $request->get('akc_club_number');

                $club->user_id          = Auth::user()->id;
                $club->save();

                \Session::flash('status', 'Your club has been updated successfully!');
                \Session::flash('alert-class', 'alert-success');
                return \Redirect::route('club.show',$club);
            }
        }else{
            \Session::flash('status', 'You do not have the permission to access this section!');
            \Session::flash('alert-class', 'alert-danger');
            return back();
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function destroy(Club $club)
    {
        //
    }
}
