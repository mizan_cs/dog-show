<?php

namespace App\Http\Controllers;

use App\Club;
use App\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->middleware('auth')->except(array('index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Club $club)
    {
        $tab = 'events';
        if (Auth::user()->clubs()->count() == 0)
        {
            \Session::flash('status', 'You need to create a club first in order to create an event.');
            \Session::flash('alert-class', 'alert-danger');
            return \Redirect::route('club.create');
        }

        if ($club->is_owner(Auth::user()))
        {
            return view('events.create', compact('club','tab'));
        }
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Club $club)
    {
        // This line will verify if the club is owned by Auth user or not
        if ($club->is_owner(Auth::user())){ // This line has end and this is a condition. ok?
            // validate
            $rules = array(
                'name' => 'required|string|max:255',
                'details' => 'required|string|max:255',
                'start_date' => 'required|string|max:255',
                'end_date' => 'required|string|max:255',
                'date_entries_open' => 'required|string|max:255',
                'date_entries_close' => 'required|string|max:255',
            );
            $validator = \Validator::make($request->all(), $rules);

            // process the validation
            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $event = new Event();
                $event->name = $request->get('name');
                $event->start_date = $request->get('start_date');
                $event->end_date = $request->get('end_date');
                $event->date_entries_open = $request->get('date_entries_open');
                $event->date_entries_close = $request->get('date_entries_close');
                $event->details = $request->get('details');
                $event->club_id = $club->id;
                $event->save();
                \Session::flash('status', 'Your event has been created');
                \Session::flash('alert-class', 'alert-success');
                return back();
            }
        }
        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        return view('events.show',compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }
}
