<?php

namespace App\Http\Controllers;

use App\Dog;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('dogs.create');
    }

    public function store(Request $request)
    {
        // validate
        $rules = array(
            'call_name' => 'required|string|max:255',
            'ukc_registration_name' => 'required|string|max:255',
            'gender' => 'required|string|max:255',
            'created_at' => 'required|string|max:255',
            'ukc_registration_number' => 'required|string|max:255',
        );
        $validator = \Validator::make($request->all(), $rules);

        // process the validation
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        } else {
            $dog = new  Dog();
            $dog->ukc_registration_number = $request->get('ukc_registration_number');
            $dog->call_name = $request->get('call_name');
            $dog->ukc_registration_name = $request->get('ukc_registration_name');
            $dog->ukc_breed = $request->get('ukc_breed');
            $dog->variety = $request->get('variety');
            $dog->gender = $request->get('gender');
            $dog->created_at = $request->get('created_at');
            $dog->search_cue = $request->get('search_cue');
            $dog->final_response = $request->get('final_response');
            $dog->ukc_permanent = $request->get('ukc_permanent');
            $dog->ukctl = $request->get('ukctl');
            $dog->ukclp = $request->get('ukclp');
            $dog->height = $request->get('height');
            $dog->akc_registration_type = $request->get('akc_registration_type');
            $dog->breeder = $request->get('breeder');
            $dog->sire = $request->get('sire');
            $dog->dam = $request->get('dam');
            $dog->akc_registration_number = $request->get('akc_registration_number');
            $dog->akc_breed_id_fk = $request->get('akc_breed_id_fk');
            if ($request->get('veteran')==null){
                $dog->veteran = false;
            }else{
                $dog->veteran = true;
            }

            if ($request->get('akc_foriegn')==null){
                $dog->akc_foriegn = false;
            }else{
                $dog->akc_foriegn = true;
            }
            
            $dog->image = "https://i.ibb.co/Ld1p9v2/mia-phoy-558468-unsplash.jpg";
            $dog->save();
            $dog->users()->attach(Auth::user());

            \Session::flash('status', 'Your dog has been saved!');
            \Session::flash('alert-class', 'alert-success');
            return back();
        }
    }

    public function publish(Dog $dog)
    {
        if ($dog->own_by_user()){
            $dog->is_publish = true;
            $dog->save();

            \Session::flash('status', 'Your dog has been publish. Now Anyone can view your dog information!');
            \Session::flash('alert-class', 'alert-success');
            return back();
        }

        \Session::flash('status', 'You can not access this request.');
        \Session::flash('alert-class', 'alert-danger');
        return back();
    }


    public function pause(Dog $dog)
    {
        if ($dog->own_by_user()){
            $dog->is_publish = false;
            $dog->save();

            \Session::flash('status', 'Your dog has been unpublished now. Only you can view your dogs information.!');
            \Session::flash('alert-class', 'alert-success');
            return back();
        }

        \Session::flash('status', 'You can not access this request.');
        \Session::flash('alert-class', 'alert-danger');
        return back();
    }

    public function edit(Dog $dog)
    {
        if ($dog->own_by_user()){
            return view('dogs.edit',compact('dog'));
        }

        \Session::flash('status', 'You can not access this request.');
        \Session::flash('alert-class', 'alert-danger');
        return back();
    }

    public function update(Dog $dog, Request $request)
    {
        if ($dog->own_by_user()){

            // validate
            $rules = array(
                'call_name' => 'required|string|max:255',
                'ukc_registration_name' => 'required|string|max:255',
                'gender' => 'required|string|max:255',
                'created_at' => 'required|string|max:255',
                'ukc_registration_number' => 'required|string|max:255',
            );
            $validator = \Validator::make($request->all(), $rules);

            // process the validation
            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $dog->ukc_registration_number = $request->get('ukc_registration_number');
                $dog->call_name = $request->get('call_name');
                $dog->ukc_registration_name = $request->get('ukc_registration_name');
                $dog->ukc_breed = $request->get('ukc_breed');
                $dog->variety = $request->get('variety');
                $dog->gender = $request->get('gender');
                $dog->created_at = $request->get('created_at');
                $dog->search_cue = $request->get('search_cue');
                $dog->final_response = $request->get('final_response');
                $dog->ukc_permanent = $request->get('ukc_permanent');
                $dog->ukctl = $request->get('ukctl');
                $dog->ukclp = $request->get('ukclp');
                $dog->height = $request->get('height');
                $dog->akc_registration_type = $request->get('akc_registration_type');
                $dog->breeder = $request->get('breeder');
                $dog->sire = $request->get('sire');
                $dog->dam = $request->get('dam');
                $dog->akc_registration_number = $request->get('akc_registration_number');
                $dog->akc_breed_id_fk = $request->get('akc_breed_id_fk');

                if ($request->get('veteran')==null){
                    $dog->veteran = false;
                }else{
                    $dog->veteran = true;
                }

                if ($request->get('akc_foriegn')==null){
                    $dog->akc_foriegn = false;
                }else{
                    $dog->akc_foriegn = true;
                }

                //$dog->image = "https://i.ibb.co/Ld1p9v2/mia-phoy-558468-unsplash.jpg";
                $dog->save();

                \Session::flash('status', 'Your dog has been updated successfully!');
                \Session::flash('alert-class', 'alert-success');
                return back();
            }
        }

        \Session::flash('status', 'You can not access this request.');
        \Session::flash('alert-class', 'alert-danger');
        return back();
    }

    public function view(Dog $dog)
    {
        if ($dog->is_publish){
            return view('dogs.view', compact('dog'));
        }else{
            if ($dog->own_by_user()){
                return view('dogs.view', compact('dog'));
            }else{
                \Session::flash('status', 'This dog is not publish');
                \Session::flash('alert-class', 'alert-danger');
                return back();
            }
        }
    }


}
