<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view(User $user)
    {
        return view('user.dashboard', compact('user'));
    }

    public function edit(User $user)
    {
        if ($user->id == Auth::user()->id){
            return view('user.edit', compact('user'));
        }else{
            \Session::flash('status', 'You do not have the permission for this request!');
            \Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }

    public function update(User $u, Request $request)
    {
        if ($u->id == Auth::id()){
            // validate
            $rules = array(
                'name' => 'required|string|max:255',
                'phone' => 'required|string|max:255',
                'city' => 'required|string|max:255',
                'address' => 'required|string|max:255',
            );
            $validator = \Validator::make($request->all(), $rules);

            // process the validation
            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $u = Auth::user();

                $u->name        = $request->get('name');
                $u->phone       = $request->get('phone');
                $u->address     = $request->get('address');
                $u->city        = $request->get('city');
                $u->state_id_fk = $request->get('city_id_fk');
                $u->zip_code    = $request->get('zip_code');
                $u->save();

                \Session::flash('status', 'Your personal information has been updated successfully!');
                \Session::flash('alert-class', 'alert-success');

                return back();
            }
        }

        \Session::flash('status', 'You do not have the permission for this request!');
        \Session::flash('alert-class', 'alert-danger');
        return back();
    }

    public function update_judge(User $u, Request $request)
    {
        if ($u->id == Auth::id() && Auth::user()->is_judge){

            $u = Auth::user();
            $u->ukc_judge_number        = $request->get('ukc_judge_number');
            $u->judge_type       = $request->get('judge_type');
            $u->judge_likes     = $request->get('judge_likes');
            $u->judge_dislikes        = $request->get('judge_dislikes');
            $u->judge_rating = $request->get('judge_rating');
            $u->jr_number    = $request->get('jr_number');
            $u->akc_jr_number    = $request->get('akc_jr_number');
            $u->akc_judge_number    = $request->get('akc_judge_number');
            $u->mr_judge_number    = $request->get('mr_judge_number');
            $u->save();

            \Session::flash('status', 'Your judge information has been updated successfully!');
            \Session::flash('alert-class', 'alert-success');

            return back();
        }

        \Session::flash('status', 'You do not have the permission for this request!');
        \Session::flash('alert-class', 'alert-danger');
        return back();
    }

    public function view_clubs(User $user)
    {
        if ($user->id != Auth::user()->id && $user->clubs()->count() == 0)
        {
            \Session::flash('status', 'Do not have any club.');
            \Session::flash('alert-class', 'alert-primary');
            return back();
        }
        $clubs = $user->clubs;
        return view('user.clubs', compact('user','clubs'));
    }

    public function view_dogs(User $user)
    {
        $dogs = $user->dogs;
        return view('user.dogs', compact('user','dogs'));
    }
}
