<?php

namespace App\Http\Controllers;

use App\Administration;
use App\Club;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdministrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Club $club)
    {

        if ($club->is_owner(Auth::user()))
        {
            $tab = 'administration';
            return view('club.add-admin',compact('club','tab'));
        }
        else
        {
            abort(404);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Club $club)
    {
        if ($club->is_owner(Auth::user()))
        {
            // validate
            $rules = array(
                'Role' => 'required|string|max:255',
                'email' => 'required|email',

            );
            $validator = \Validator::make($request->all(), $rules);

            // process the validation
            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }else{

                foreach ($club->administrations as $a)
                {
                    if ($a->user->email == $request->get('email'))
                    {
                        \Session::flash('status', 'This user already exist to your administration list!');
                        \Session::flash('alert-class', 'alert-success');
                        return \Redirect::route('club.administration',$club);
                    }
                }

                \Session::flash('status', 'An Invitation Mail has been sent to the '.$request->get('email').' for the role of '.$request->get('Role'));
                \Session::flash('alert-class', 'alert-success');
                return \Redirect::route('club.administration',$club);
            }
        }
        else
        {
            abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Administration  $administration
     * @return \Illuminate\Http\Response
     */
    public function show(Administration $administration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Administration  $administration
     * @return \Illuminate\Http\Response
     */
    public function edit(Administration $administration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Administration  $administration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Administration $administration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Administration  $administration
     * @return \Illuminate\Http\Response
     */
    public function destroy(Administration $administration)
    {
        //
    }
}
