<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Dog extends Model
{
    protected $fillable = [
        'ukc_registration_number', 'call_name', 'ukc_registration_name', 'ukc_breed','variety','gender','notes','attachment','search_cue','final_response','image','ukc_permanent','ukctl','ukclp','height','veteran','akc_registration_number','akc_registration_type','akc_foriegn','akc_breed_id_fk','breeder','sire','dam','is_publish','created_at'
    ];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function own_by_user()
    {
        foreach ($this->users as $user){
            if ($user->id == Auth::user()->id){
                return true;
            }
        }
        return false;
    }
}
