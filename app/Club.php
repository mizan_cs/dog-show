<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function is_owner($user)
    {
        return $user->id == $this->user->id;
    }

    public function events()
    {
        return $this->hasMany('App\Event')->orderBy('start_date');
    }

    public function active_events()
    {
        return $this->hasMany('App\Event')->where('is_active',true);
    }

    public function upcoming_event()
    {
        return $this->events()->where('start_date','>',Carbon::today()->toDateString())->orderBy('start_date')->first();
    }
    public function upcoming_events()
    {
        return $this->events()->where('start_date','>',Carbon::today()->toDateString())->orderBy('start_date')->where('is_active',true)->get();
    }

    public function administrations()
    {
        return $this->hasMany('App\Administration');
    }

}
