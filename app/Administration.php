<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Administration extends Model
{
    public function club()
    {
        return $this->belongsTo('App\Club');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
