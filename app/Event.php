<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Event extends Model
{
    public function club()
    {
        return $this->belongsTo('App\Club');
    }

    public function is_owned()
    {
        return $this->club->user->id == Auth::user()->id;
    }

    public function status()
    {
        //Carbon::parse($this->start_date)->diffForHumans()
        //between(Carbon::parse($this->start_date)->format('Y-m-d')
//        if (Carbon::now()->toDateString() >= Carbon::parse($this->start_date)->format('Y-m-d') && Carbon::now()->toDateString()  Carbon::parse($this->end_date)->format('Y-m-d'))
//        {
//            return 'Running';
//        }

        if (Carbon::now()->toDateString() < Carbon::parse($this->start_date)->format('Y-m-d'))
        {
            return 'Upcoming';
        }
        else
        {
            return 'End';
        }

    }
}
