@extends('layouts.theme')

@section('content')


    <div class="main-container">
        <section class="bg-dark text-white height-70">
            <img alt="Image" src="http://www.stuntdogshow.com/data1_fairs/images/stuntdog_dixon_mayfair_2013111.jpg" class="bg-image opacity-70" />
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-9 col-lg-5">
                        <h1 class="display-3">Ready, Set, Grow. -Dog</h1>
                        <span class="lead">A robust suite of styled components, powered by Bootstrap 4.  Take the design of your website or webapp up a notch.</span>
                        <a href="#" class="btn btn-success btn-lg">Browse Events Now</a>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <!--end of section-->
        <section>
            <div class="container">
                <div class="row justify-content-between mb-5">
                    <div class="col-auto">
                        <h3>Upcoming Events</h3>
                    </div>
                    <!--end of col-->
                    <div class="col-auto">
                        <a href="#" class="btn btn-outline-primary">View all</a>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
                <ul class="row feature-list feature-list-sm">

                    <li class="col-12 col-md-6 col-lg-4">
                        <div class="card">
                            <a href="#">
                                <img class="card-img-top" src="assets/img/graphic-product-sidekick.jpg" alt="Card image cap">
                            </a>
                            <div class="card-body">
                                <a href="#">
                                    <h4 class="card-title">Sidekick</h4>
                                    <p class="card-text text-body">Holistic fitness tracking</p>
                                </a>
                            </div>
                            <div class="card-footer card-footer-borderless d-flex justify-content-between">
                                <div class="text-small">
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><i class="icon-heart"></i> 221</li>
                                        <li class="list-inline-item"><i class="icon-message"></i> 14</li>
                                    </ul>
                                </div>
                                <div class="dropup">
                                    <button class="btn btn-sm btn-outline-primary dropdown-toggle dropdown-toggle-no-arrow" type="button" id="SidekickButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="icon-dots-three-horizontal"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-sm" aria-labelledby="SidekickButton">
                                        <a class="dropdown-item" href="#">Save</a>
                                        <a class="dropdown-item" href="#">Share</a>
                                        <a class="dropdown-item" href="#">Comment</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Report</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <!--end of col-->

                    <li class="col-12 col-md-6 col-lg-4">
                        <div class="card">
                            <a href="#">
                                <img class="card-img-top" src="assets/img/graphic-product-pitstop.jpg" alt="Card image cap">
                            </a>
                            <div class="card-body">
                                <a href="#">
                                    <h4 class="card-title">Pitstop</h4>
                                    <p class="card-text text-body">Browser-based project management</p>
                                </a>
                            </div>
                            <div class="card-footer card-footer-borderless d-flex justify-content-between">
                                <div class="text-small">
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><i class="icon-heart"></i> 90</li>
                                        <li class="list-inline-item"><i class="icon-message"></i> 34</li>
                                    </ul>
                                </div>
                                <div class="dropup">
                                    <button class="btn btn-sm btn-outline-primary dropdown-toggle dropdown-toggle-no-arrow" type="button" id="PitstopButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="icon-dots-three-horizontal"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-sm" aria-labelledby="PitstopButton">
                                        <a class="dropdown-item" href="#">Save</a>
                                        <a class="dropdown-item" href="#">Share</a>
                                        <a class="dropdown-item" href="#">Comment</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Report</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <!--end of col-->

                    <li class="col-12 col-md-6 col-lg-4">
                        <div class="card">
                            <a href="#">
                                <img class="card-img-top" src="assets/img/graphic-product-pipeline.jpg" alt="Card image cap">
                            </a>
                            <div class="card-body">
                                <a href="#">
                                    <h4 class="card-title">pipeline.js</h4>
                                    <p class="card-text text-body">Snappy UI interaction library with flexible API</p>
                                </a>
                            </div>
                            <div class="card-footer card-footer-borderless d-flex justify-content-between">
                                <div class="text-small">
                                    <ul class="list-inline">
                                        <li class="list-inline-item"><i class="icon-heart"></i> 84</li>
                                        <li class="list-inline-item"><i class="icon-message"></i> 25</li>
                                    </ul>
                                </div>
                                <div class="dropup">
                                    <button class="btn btn-sm btn-outline-primary dropdown-toggle dropdown-toggle-no-arrow" type="button" id="pipeline.jsButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="icon-dots-three-horizontal"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-sm" aria-labelledby="pipeline.jsButton">
                                        <a class="dropdown-item" href="#">Save</a>
                                        <a class="dropdown-item" href="#">Share</a>
                                        <a class="dropdown-item" href="#">Comment</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Report</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <!--end of col-->

                </ul>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <!--end of section-->
        <section>
            <div class="container">
                <div class="row justify-content-between mb-5">
                    <div class="col-auto">
                        <h3>Clubs</h3>
                    </div>
                    <!--end of col-->
                    <div class="col-auto">
                        <a href="#" class="btn btn-outline-primary">View all</a>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
                <ul class="feature-list feature-list-sm row">

                    <li class="col-12 col-md-6 col-lg-4">
                        <div class="card card-lg">
                            <a href="#">
                                <img class="card-img-top" src="assets/img/photo-team-desk.jpg" alt="Starting Up - A Candid Documentary">
                            </a>
                            <div class="card-body">
                                <a href="#">
                                    <h4 class="card-title mb-3">Starting Up - A Candid Documentary</h4>
                                </a>
                                <p class="card-text">What it’s really like trying to convert an idea into a business.</p>
                            </div>
                            <div class="card-footer card-footer-borderless">
                                <div class="media blog-post-author">
                                    <img alt="Image" src="assets/img/avatar-male-1.jpg" class="avatar avatar-xs" />
                                    <div class="media-body">
                                        <small>Cameron Vasquez</small>
                                        <small>28th February 2018 &bull; 4 min read</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="col-12 col-md-6 col-lg-4">
                        <div class="card card-lg">
                            <a href="#">
                                <img class="card-img-top" src="assets/img/photo-man-box.jpg" alt="There is no competition">
                            </a>
                            <div class="card-body">
                                <a href="#">
                                    <h4 class="card-title mb-3">There is no competition</h4>
                                </a>
                                <p class="card-text">A new businesses biggest enemy is feature-envy</p>
                            </div>
                            <div class="card-footer card-footer-borderless">
                                <div class="media blog-post-author">
                                    <img alt="Image" src="assets/img/avatar-female-1.jpg" class="avatar avatar-xs" />
                                    <div class="media-body">
                                        <small>Rebecca Canning</small>
                                        <small>29th February 2018 &bull; 3 min read</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="col-12 col-md-6 col-lg-4">
                        <div class="card card-lg">
                            <a href="#">
                                <img class="card-img-top" src="assets/img/photo-man-book.jpg" alt="Best practices in form design">
                            </a>
                            <div class="card-body">
                                <a href="#">
                                    <h4 class="card-title mb-3">Best practices in form design</h4>
                                </a>
                                <p class="card-text">Build forms like a boss</p>
                            </div>
                            <div class="card-footer card-footer-borderless">
                                <div class="media blog-post-author">
                                    <img alt="Image" src="assets/img/avatar-male-2.jpg" class="avatar avatar-xs" />
                                    <div class="media-body">
                                        <small>Luke Freebody</small>
                                        <small>24th February 2018 &bull; 2 min read</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                </ul>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <!--end of section-->

        <section>
            <div class="container">
                <div class="row justify-content-center text-center section-intro">
                    <div class="col-12 col-md-9 col-lg-8">
                        <span class="title-decorative">Meet The Dogs</span>
                        <h2 class="display-4">Introducing a new way</h2>
                        <span class="lead">An opportunity to introduce the major benefits of your product and set the scene for what's to come</span>

                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
                <div class="row justify-content-center">
                    <div class="col-md-8 col-sm-10 text-center">
                        <iframe width="660" height="415" src="https://www.youtube.com/embed/JUXgSN6arpE?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <!--end of video cover-->
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
                <div class="row justify-content-center text-center section-outro">
                    <div class="col-lg-4 col-md-5">
                        <h6>Introducing a new way</h6>
                        <p>An opportunity to introduce the major benefits of your product and set the scene for what's to come</p>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <!--end of section-->

        <section class="space-xlg bg-gradient text-white">
            <div class="container">
                <div class="row text-center">
                    <div class="col">
                        <h3 class="h1">Start your clubs here</h3>
                        <a href="{{route('club.create')}}" class="btn btn-lg btn-success">Create Club</a>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <!--end of section-->
        <footer class="bg-gray text-light footer-long">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-3">
                        <img alt="Image" src="assets/img/logo-white.svg" class="mb-4" />
                        <p class="text-muted">
                            &copy; 2019 Company Name
                            <br />Company Tagline
                            <br />at Company Address
                        </p>
                    </div>
                    <!--end of col-->
                    <div class="col-12 col-md-9">
                        <span class="h5">A robust suite of styled components for Bootstrap 4</span>
                        <div class="row no-gutters">
                            <div class="col-6 col-lg-3">
                                <h6>Navigate</h6>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="index.html">Overview</a>
                                    </li>
                                    <li>
                                        <a href="pages-landing.html">Landing Pages</a>
                                    </li>
                                    <li>
                                        <a href="pages-app.html">App Pages</a>
                                    </li>
                                    <li>
                                        <a href="pages-inner.html">Inner Pages</a>
                                    </li>
                                </ul>
                            </div>
                            <!--end of col-->
                            <div class="col-6 col-lg-3">
                                <h6>Platform</h6>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="#">Mac OS &amp; iOS</a>
                                    </li>
                                    <li>
                                        <a href="#">Android &amp; Chrome OS</a>
                                    </li>
                                    <li>
                                        <a href="#">Windows</a>
                                    </li>
                                    <li>
                                        <a href="#">Linux</a>
                                    </li>
                                </ul>
                            </div>
                            <!--end of col-->
                            <div class="col-6 col-lg-3">
                                <h6>Community</h6>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="#">Forum</a>
                                    </li>
                                    <li>
                                        <a href="#">Knowledgebase</a>
                                    </li>
                                    <li>
                                        <a href="#">Hire an expert</a>
                                    </li>
                                    <li>
                                        <a href="#">FAQ</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>
                            <!--end of col-->
                            <div class="col-6 col-lg-3">
                                <h6>Company</h6>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="#">About company</a>
                                    </li>
                                    <li>
                                        <a href="#">History</a>
                                    </li>
                                    <li>
                                        <a href="#">Team</a>
                                    </li>
                                    <li>
                                        <a href="#">Investment</a>
                                    </li>
                                </ul>
                            </div>
                            <!--end of col-->
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </footer>
    </div>
@endsection



