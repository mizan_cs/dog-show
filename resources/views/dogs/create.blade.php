@extends('dogs.base')

@section('dog-content')
    <div class="card-body">
        <h6 class="title-decorative text-center">Create New Dogs</h6>
        <form class="form" method="post" action="{{route('dog.store')}}">
            {{csrf_field()}}
            <div class="form-group">
                <label for="ukc_registration_number">UKC Registration Number <code>*</code></label>
                <input type="text" class="form-control" name="ukc_registration_number" id="ukc_registration_number" placeholder="UKC Registration Number" required>
                @if ($errors->has('ukc_registration_number'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('ukc_registration_number') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="call_name">Dog's Call Name <code>*</code></label>
                <input type="text" class="form-control" name="call_name" id="call_name" placeholder="Dog's Call Name" required>
                @if ($errors->has('call_name'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('call_name') }}</strong>
                    </span>
                @endif
            </div>


            <div class="form-group">
                <label for="ukc_registration_name">UKC Registered Name <code>*</code></label>
                <input type="text" class="form-control" name="ukc_registration_name" id="ukc_registration_name" placeholder="UKC Registered Name" required>
                @if ($errors->has('ukc_registration_name'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('ukc_registration_name') }}</strong>
                    </span>
                @endif
            </div>


            <div class="form-group">
                <label for="ukc_breed">UKC Breed</label>
                <input type="text" class="form-control" name="ukc_breed" id="ukc_breed" placeholder="UKC Breed">
                @if ($errors->has('ukc_breed'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('ukc_breed') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="variety">Breed variety if any</label>
                <input type="text" class="form-control" name="variety" id="variety" placeholder="Breed variety if any">
                @if ($errors->has('variety'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('variety') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="gender">Dog's Gender <code>*</code></label>
                <select class="form-control" name="gender" required>
                    <option value="male" selected>Mele</option>
                    <option value="female">Female</option>
                </select>
                @if ($errors->has('gender'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('gender') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="created_at">Date of Birth <code>*</code></label>
                <input type="date" class="form-control" name="created_at" id="created_at" placeholder="Date of Birth" required>
                @if ($errors->has('created_at'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('created_at') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="search_cue">Search Command in Nosework</label>
                <input type="text" class="form-control" name="search_cue" id="search_cue" placeholder="Search Command in Nosework">
                @if ($errors->has('search_cue'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('search_cue') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="final_response">Dogs Alert Indication in Nosework</label>
                <input type="text" class="form-control" name="final_response" id="final_response" placeholder="Dogs Alert Indication in Nosework">
                @if ($errors->has('final_response'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('final_response') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="ukc_permanent">UKC Permanent Registration Number</label>
                <input type="text" class="form-control" name="ukc_permanent" id="ukc_permanent" placeholder="UKC Permanent Registration Number">
                @if ($errors->has('ukc_permanent'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('ukc_permanent') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="ukctl">UKC Temporary Registration Number</label>
                <input type="text" class="form-control" name="ukctl" id="ukctl" placeholder="UKC Temporary Registration Number">
                @if ($errors->has('ukctl'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('ukctl') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="ukclp">UKC Limited Registration Number</label>
                <input type="text" class="form-control" name="ukclp" id="ukclp" placeholder="UKC Limited Registration Number">
                @if ($errors->has('ukclp'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('ukclp') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="height">Height at Withers</label>
                <input type="number" class="form-control" name="height" id="height" placeholder="Height at Withers">
                @if ($errors->has('height'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('height') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="veteran" id="veteran">
                    <label class="custom-control-label" for="veteran">Dog is a Veteran?</label>
                </div>
                @if ($errors->has('veteran'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('veteran') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="akc_registration_number">AKC Registration Number</label>
                <input type="text" class="form-control" name="akc_registration_number" id="akc_registration_number" placeholder="AKC Registration Number">
                @if ($errors->has('akc_registration_number'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('akc_registration_number') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="akc_registration_type">AKC Registration Type</label>
                <select class="form-control" name="akc_registration_type">
                    <option value="AKC" selected>AKC</option>
                    <option value="PAL">PAL</option>
                    <option value="ILP">ILP</option>
                    <option value="CP">CP</option>
                </select>
                @if ($errors->has('akc_registration_type'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('akc_registration_type') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="akc_foriegn" id="akc_foriegn">
                    <label class="custom-control-label" for="akc_foriegn">AKC Registration Foriegn</label>
                </div>
                @if ($errors->has('akc_foriegn'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('akc_foriegn') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="akc_breed_id_fk">AKC Breed</label>
                <input type="text" class="form-control" name="akc_breed_id_fk" id="akc_breed_id_fk" placeholder="AKC Breed">
                @if ($errors->has('AKC Breed'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('AKC Breed') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="breeder">Name of Dog's Breeder</label>
                <input type="text" class="form-control" name="breeder" id="breeder" placeholder="Name of Dog's Breeder">
                @if ($errors->has('breeder'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('breeder') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="sire">Sire of This Dog</label>
                <input type="text" class="form-control" name="sire" id="sire" placeholder="Sire of This Dog">
                @if ($errors->has('sire'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('sire') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="sire">Dam of This Dog</label>
                <input type="text" class="form-control" name="dam" id="dam" placeholder="Dam of This Dog">
                @if ($errors->has('dam'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('dam') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group text-center">
                <button class="btn btn-primary" type="submit">Save Dog</button>
            </div>


        </form>
    </div>
@endsection