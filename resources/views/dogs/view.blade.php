@extends('dogs.base')

@section('dog-content')
    <div class="main-container">

        <section class="bg-white space-sm">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 mb-4 mb-sm-0">
                        <img alt="Image" src="{{$dog->image}}" class="rounded img-fluid">
                    </div>
                    <!--end of col-->
                    <div class="col-lg-4 d-flex flex-column justify-content-between mr-auto ml-auto">
                        <div>
                            <h1 class="mb-2">{{$dog->call_name}}</h1>
                            <h6 class="mb-1">UKC registration number: {{$dog->ukc_registration_number}}</h6>
                            <h6 class="mb-1">UKC registration name: {{$dog->ukc_registration_name}}</h6>
                            <div>
                                <span class="badge badge-secondary mr-3">{{$dog->gender}}</span>
                            </div>
                        </div>
                        <div>
                            <a class="btn btn-success btn-sm btn-block" href="#" role="button">Contact Owner</a>
                        </div>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <!--end of section-->
        <section class="space-sm">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-12 col-md-12 col-lg-12">
                        <h5 class="mb-4 text-center">Overview</h5>
                        <div class="card">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>UKC Breed</div>
                                        <span>@if($dog->ukc_breed){{$dog->ukc_breed}} @else N/A @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>Breed Variety</div>
                                        <span>@if($dog->variety) {{$dog->variety}} @else N/A @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>Date of Birth</div>
                                        <span>@if($dog->created_at->toFormattedDateString()) {{$dog->created_at->toFormattedDateString()}} @else N/A @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>Search Command in Nosework</div>
                                        <span>@if($dog->search_cue) {{$dog->search_cue}} @else N/A @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>Dogs Alert Indication in Nosework</div>
                                        <span>@if($dog->final_response) {{$dog->final_response}} @else N/A @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>UKC Permanent Registration Number</div>
                                        <span>@if($dog->ukc_permanent){{$dog->ukc_permanent}} @else N/A @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>UKC Temporary Registration Number</div>
                                        <span>@if($dog->ukctl){{$dog->ukctl}} @else N/A @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>UKC Limited Registration Number</div>
                                        <span>@if($dog->ukclp) {{$dog->ukclp}} @else N/A @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>Height at Withers</div>
                                        <span>@if($dog->height) {{$dog->height}} @else N/A @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>Dog Is a Veteran?</div>
                                        <span>@if($dog->veteran)Yes @else NO @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>AKC Registration Number</div>
                                        <span>@if($dog->akc_registration_number) {{$dog->akc_registration_number}} @else N/A @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>AKC Registration Type</div>
                                        <span>@if($dog->akc_registration_type) {{$dog->akc_registration_type}} @else N/A @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>AKC Registration Foriegn</div>
                                        <span>@if($dog->akc_foriegn)Yes @else No @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>AKC Breed</div>
                                        <span>@if($dog->akc_breed_id_fk) {{$dog->akc_breed_id_fk}} @else N/A @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>Name of Dog's Breeder</div>
                                        <span>@if($dog->breeder) {{$dog->breeder}} @else N/A @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>Sire of This Dog</div>
                                        <span>@if($dog->sire) {{$dog->sire}} @else N/A @endif</span>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="d-flex justify-content-between">
                                        <div>Dam of This Dog</div>
                                        <span>@if($dog->dam) {{$dog->dam }} @else N/A @endif</span>
                                    </div>
                                </li>

                            </ul>
                        </div>
                        <a class="btn btn-success btn-sm btn-block" href="{{ URL::previous() }}" role="button">Back</a>

                    </div>

                    <!--end of col-->
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>

    </div>
@endsection