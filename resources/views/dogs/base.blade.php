@extends('layouts.theme')

@section('content')

    <div class="main-container">
        <section>
            <div class="container">

                <div class="row justify-content-center">
                    <div class="col-12 col-lg-10">
                        @if (session('status'))
                            <div class="alert {{ Session::get('alert-class', 'alert-info') }}" role="alert">
                                {!! session('status') !!}
                            </div>
                        @endif
                        <div class="card card-lg w-100">
                            @yield('dog-content')
                        </div>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <!--end of section-->
    </div>

@endsection