<div class="navbar-container">
        <div class="bg-dark navbar-dark position-fixed" data-sticky="top">
            <div class="container">
                <nav class="navbar navbar-expand-lg">
                    <a class="navbar-brand" href="{{route('home')}}">
                        <img alt="Wingman" src="{{asset('assets/img/logo-white.svg')}}" />
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="icon-menu h4"></i>
                    </button>
                    <div class="collapse navbar-collapse justify-content-between" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a href="#" class="nav-link">Events</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" role="button">Clubs</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" role="button">About us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" role="button">Contact us</a>
                            </li>
                        </ul>
                    @if (Route::has('login'))
                        <ul class="navbar-nav">
                            @auth
                                <li class="nav-item dropdown">
                                                  <a class="nav-link dropdown-toggle dropdown-toggle-no-arrow p-lg-0" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    {{ Auth::user()->name }}
                                                    <img alt="Image" src="{{asset('assets/img/avatar-male-3.jpg')}}" class="avatar avatar-xs">
                                                  </a>
                                                  <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" aria-labelledby="dropdown01">
                                                    <a class="dropdown-item" href="{{route('user.view',Auth::user()->id)}}">Profile</a>
                                                    <a class="dropdown-item" href="{{route('user.view.dogs',Auth::user())}}">Dogs</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="dropdown-item" href="#">Settings</a>
                                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                                    Log out</a>
                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                                                            @csrf
                                                                                        </form>
                                                  </div>
                                 </li>
                            @else
                                <li class="nav-item">
                                    <a href="{{ route('register') }}">Sign up</a>
                                    <span>&nbsp;or&nbsp;</span><a href="{{ route('login') }}">Sign in</a>
                                </li>
                            @endauth
                        </ul>
                     @endif

                    </div>
                    <!--end nav collapse-->
                </nav>
            </div>
            <!--end of container-->
        </div>
    </div>


