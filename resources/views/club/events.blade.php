@extends('club.show')

@section('club-nav-content')
    @if($club->is_owner(Auth::user()))
        <div class="container">
            <a class="btn btn-primary btn-block mt-4" href="{{route('event.create',$club)}}" role="button">Create Event</a>
        </div>
    @endif
    <div class="container mt-4">
    @if($club->upcoming_event() != null)
            <div class="card">
                <div class="card-header card-header-borderless d-flex justify-content-between">
                    <small class="text-muted"><i class="icon-calendar mr-1"></i> Upcoming Event</small>
                    <small class="text-muted">{{\Carbon\Carbon::parse($club->upcoming_event()->start_date)->diffForHumans()}}</small>
                </div>
                <div class="card-body">
                    <div class="media">
                        <img alt="Image" src="{{asset('assets/img/graphic-product-kin-thumb.jpg')}}" class="avatar avatar-square">
                        <div class="media-body">
                            <span class="h5 mb-0">{{$club->upcoming_event()->name}}</span>
                            <span>The digital fashion assistant</span>
                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-between">
                    <div>
                        <button class="btn btn-sm btn-outline-secondary">Manage</button>
                    </div>
                    <div class="text-small">
                        <ul class="list-inline">
                            <a class="btn btn-primary btn-sm" href="{{route('event.show',$club->upcoming_event())}}" role="button">Join Now</a>
                        </ul>
                    </div>
                </div>
            </div>
    @endif

    <br>
    <h4 class="text-center">All Events</h4>
    <ul class="row feature-list feature-list-sm">
        @forelse($club->active_events as $event)
            <li class="col-12 col-md-6 col-lg-4">
                <div class="card">
                    <a href="{{route('event.show',$event)}}">
                        <img class="card-img-top" src="https://i.ibb.co/NS7Hnsj/alexander-popov-1139372-unsplash.jpg" alt="Card image cap">
                    </a>
                    <div class="card-body">
                        <a href="{{route('event.show',$event)}}">
                            <h4 class="card-title">{{$event->name}}</h4>
                            <p class="card-text text-body">Accounting for creative people</p>
                        </a>
                    </div>
                    <div class="card-footer card-footer-borderless d-flex justify-content-between">
                        <div class="text-small">
                            <ul class="list-inline">
                                <li class="list-inline-item"><i class="icon-calendar mr-1"></i> {{\Carbon\Carbon::parse($event->start_date)->toFormattedDateString()}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
        @empty
            <div class="text-center">
                <h2 class="text-center">No event to join</h2>
            </div>

        @endforelse

        <!--end of col-->


    </ul>
    <!--end of row-->
</div>
@endsection