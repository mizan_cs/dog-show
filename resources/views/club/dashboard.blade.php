@extends('club.show')

@section('club-nav-content')
    <div class="main-container">
        <!--end of section-->
        <section class="bg-white space-sm flush-with-above">
            <div class="container">
                <div class="row mb-3">
                    <div class="col">
                        <div class="progress">
                            <div class="progress-bar bg-success opacity-50" role="progressbar" style="width: 25%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                            <div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
                <div class="row text-center">
                    <div class="col">
                        <span>Active Shows</span>
                        <br>
                        <small class="text-muted">{{$club->events()->where('is_active',1)->count()}}</small>
                    </div>
                    <!--end of col-->
                    <div class="col">
                        <span>Upcoming Show</span>
                        <br>

                        <small class="text-muted"><i class="icon-calendar mr-1"></i>@if($club->upcoming_event() != null) {{\Carbon\Carbon::parse($club->upcoming_event()->start_date)->format('d-m-Y')}} @else No Upcoming Events @endif</small>
                    </div>
                    <!--end of col-->
                    <div class="col">
                        <span>Total Booking</span>
                        <br>
                        <small class="text-muted"><i class="icon-bar-graph mr-1"></i>$12000 USD</small>
                    </div>
                    <!--end of col-->
                    <div class="col">
                        <span>People Reach</span>
                        <br>
                        <small class="text-muted"><i class="icon-area-graph mr-1"></i>1.5k</small>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
                <hr>
            </div>
            <!--end of container-->
        </section>
        <!--end of section-->
        <section class="flush-with-above">
            <div class="container">
                <h4 class="text-center">All Shows</h4>
                <div class="row">
                    <div class="col">
                        <table class="table table-borderless table-hover align-items-center">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Start Date</th>
                                <th scope="col">End Date</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($club->events as $event)
                                @if($event->is_active || $event->is_owned())
                                    <tr class="bg-white">
                                        <th scope="row">
                                            <a class="media align-items-center" href="{{route('event.show',$event)}}">

                                                <div class="media-body">
                                                    <span class="h6 mb-0">{{$event->name}}</span>
                                                </div>
                                            </a>
                                        </th>
                                        <td>{{\Carbon\Carbon::parse($event->start_date)->format('d-m-Y')}}</td>
                                        <td>{{\Carbon\Carbon::parse($event->end_date)->format('d-m-Y')}}</td>
                                        <td>
                                            <span class="badge @if($event->is_active) badge-success @else badge-danger @endif"> @if($event->is_active) Active @else Not Active @endif</span>
                                        </td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-sm btn-outline-primary dropdown-toggle dropdown-toggle-no-arrow" type="button" id="dropdownMenuButton-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="icon-dots-three-horizontal"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-sm">
                                                    <a class="dropdown-item" href="{{route('event.show',$event)}}">View</a>
                                                    <a class="dropdown-item" href="{{}}">Edit</a>
                                                    <div class="dropdown-divider"></div>
                                                    {{--<a class="dropdown-item text-danger" href="{{}}">Disable</a>--}}
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="table-divider">
                                        <th></th>
                                        <td></td>
                                    </tr>
                                @endif
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
    </div>
@endsection