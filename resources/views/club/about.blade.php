@extends('club.show')

@section('club-nav-content')
    <section class="space-sm">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-12 col-md-12 col-lg-12">
                    <h5 class="mb-4 text-center">Club Overview</h5>
                    <div class="card">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div class="d-flex justify-content-between">
                                    <div>Club Name</div>
                                    <span>{{$club->name}}</span>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="d-flex justify-content-between">
                                    <div>Club's UKC Number</div>
                                    <span> {{$club->club_number}} </span>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="d-flex justify-content-between">
                                    <div>Club Email</div>
                                    <span> {{$club->email}} </span>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="d-flex justify-content-between">
                                    <div>Club Website</div>
                                    <span>@if($club->website == null) N/A @else {{$club->website}} @endif </span>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="d-flex justify-content-between">
                                    <div>Club Phone Number</div>
                                    <span> {{$club->phone}} </span>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="d-flex justify-content-between">
                                    <div>Club Address </div>
                                    <span>{{$club->address}} </span>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="d-flex justify-content-between">
                                    <div>City Name</div>
                                    <span>{{$club->city}}</span>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="d-flex justify-content-between">
                                    <div>State ID</div>
                                    <span>@if($club->state_id_fk == null) N/A @else {{$club->state_id_fk}} @endif</span>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="d-flex justify-content-between">
                                    <div>Zip Code</div>
                                    <span> {{$club->zip_code}} </span>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="d-flex justify-content-between">
                                    <div>AKC Club Name</div>
                                    <span>@if($club->akc_club_name == null) N/A @else {{$club->akc_club_name}} @endif  </span>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="d-flex justify-content-between">
                                    <div>AKC Club Number</div>
                                    <span> @if($club->akc_club_number == null) N/A @else {{$club->akc_club_number}} @endif </span>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

                <!--end of col-->
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
@endsection