@extends('club.show')

@section('club-nav-content')
    <div class="main-container">

        <section class="flush-with-above">
            <div class="container">
                <h4 class="text-center">Club Administration</h4>
                <div class="row">
                    <div class="col">
                        <table class="table table-borderless table-hover align-items-center">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            @foreach($club->administrations as $admin)
                                <tbody>
                                <tr class="bg-white">
                                    <th scope="row">
                                        <a class="media align-items-center" href="{{route('user.view',$admin->user)}}">
                                            <div class="media-body">
                                                <span class="h6 mb-0">{{$admin->user->name}}</span>
                                            </div>
                                        </a>
                                    </th>
                                    <td>
                                        <span class="badge badge-success"> {{$admin->Role}}</span>
                                    </td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-sm btn-outline-primary dropdown-toggle dropdown-toggle-no-arrow" type="button" id="dropdownMenuButton-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="icon-dots-three-horizontal"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-sm">
                                                <a class="dropdown-item" href="">View</a>
                                                <a class="dropdown-item" href="">Edit</a>
                                                <div class="dropdown-divider"></div>
                                                {{--<a class="dropdown-item text-danger" href="{{}}">Disable</a>--}}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="table-divider">
                                    <th></th>
                                    <td></td>
                                </tr>
                                </tbody>
                            @endforeach
                        </table>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>

        @if($club->is_owner(Auth::user()))
            <div class="container">
                <a class="btn btn-primary btn-block mt-4" href="{{route('administration.add',$club)}}" role="button">Add User</a>
            </div>
        @endif

    </div>
@endsection