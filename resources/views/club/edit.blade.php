@extends('club.base')

@section('club-content')

    <div class="card-body">
        <h6 class="title-decorative text-center">Update Club Information</h6>
        <form class="form" method="post" action="{{route('club.update',$club)}}">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">Club Name<code>*</code></label>
                <input type="text" class="form-control" name="name" id="name" value="{{$club->name}}" placeholder="Club Name" required>
                @if ($errors->has('name'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="club_number">Club UKC Number <code>*</code></label>
                <input type="text" class="form-control" name="club_number" id="club_number" value="{{$club->club_number}}" placeholder="Club UKC Number" required>
                @if ($errors->has('club_number'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('club_number') }}</strong>
                    </span>
                @endif
            </div>


            <div class="form-group">
                <label for="email">Club Email<code>*</code></label>
                <input type="email" class="form-control" name="email" value="{{$club->email}}" id="email" placeholder="Club Email" required>
                @if ($errors->has('email'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>


            <div class="form-group">
                <label for="website">Club Website</label>
                <input type="text" class="form-control" name="website" id="website" value="{{$club->website}}" placeholder="Club Website">
                @if ($errors->has('website'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('website') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="phone">Club Phone Number<code>*</code></label>
                <input type="text" class="form-control" name="phone" value="{{$club->phone}}" id="phone" placeholder="Club Phone Number">
                @if ($errors->has('phone'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="address">Club Address <code>*</code></label>
                <input type="text" class="form-control" name="address" id="address" value="{{$club->address}}" placeholder="Club Address" required>
                @if ($errors->has('address'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="city">City Name <code>*</code></label>
                <input type="text" class="form-control" name="city" id="city" value="{{$club->city}}" placeholder="City Name" required>
                @if ($errors->has('city'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('city') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="state_id_fk">State ID</label>
                <input type="text" class="form-control" name="state_id_fk" value="{{$club->state_id_fk}}" id="state_id_fk" placeholder="State ID">
                @if ($errors->has('state_id_fk'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('state_id_fk') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="zip_code">Zip Code<code>*</code></label>
                <input type="text" class="form-control" name="zip_code" value="{{$club->zip_code}}" id="zip_code" placeholder="Zip Code">
                @if ($errors->has('zip_code'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('zip_code') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="akc_club_name">AKC Club Name</label>
                <input type="text" class="form-control" value="{{$club->akc_club_name}}" name="akc_club_name" id="akc_club_name" placeholder="AKC Club Name">
                @if ($errors->has('akc_club_name'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('akc_club_name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="akc_club_number">AKC Club Number</label>
                <input type="text" class="form-control" name="akc_club_number" value="{{$club->akc_club_number}}" id="akc_club_number" placeholder="AKC Club Number">
                @if ($errors->has('akc_club_number'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('akc_club_number') }}</strong>
                    </span>
                @endif
            </div>


            <div class="form-group text-center">
                <a class="btn btn-secondary" href="{{URL::previous()}}" role="button">Back</a>
                <button class="btn btn-primary" type="submit">Update Club</button>
            </div>

        </form>
    </div>
@endsection