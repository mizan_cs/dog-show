@extends('club.show')

@section('club-nav-content')
    <div class="main-container">
        <section class="flush-with-above">
            <div class="container">
                <h4 class="text-center">Add a user to club administration</h4>
                <form class="form" method="post" action="{{route('administration.store',$club)}}">
                    @if (session('status'))
                        <div class="alert {{ Session::get('alert-class', 'alert-info') }}" role="alert">
                            {!! session('status') !!}
                        </div>
                    @endif
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="email">User Email<code>*</code></label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="User Email" required>
                        @if ($errors->has('email'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="Role">User Role<code>*</code></label>
                        <input type="text" class="form-control" name="Role" id="Role" placeholder="User Role" required>
                        @if ($errors->has('Role'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('Role') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group text-center">
                        <button class="btn btn-primary" type="submit">Add user to club administration</button>
                    </div>


                </form>
            </div>
            <!--end of container-->
        </section>


    </div>
@endsection