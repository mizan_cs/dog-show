@extends('club.base')
@section('club-content')
    <div class="main-container">
        <section class="bg-dark text-light height-50">
            <img alt="Image" src="@if($club->image){{$club->image}}@else {{'https://i.ibb.co/NS7Hnsj/alexander-popov-1139372-unsplash.jpg'}} @endif" class="bg-image opacity-70">
            <div class="container align-self-end">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <div class="media">
                            <img alt="Image" src="@if($club->image){{$club->image}}@else {{'https://www.designevo.com/res/templates/thumb_small/red-baseball-club.png'}} @endif" class="mr-md-4 avatar avatar-lg avatar-square">
                            <div class="media-body">
                                <div class="mb-3">
                                    <h1 class="h3 mb-2">{{$club->name}}</h1>
                                    <span class="text-muted">Designer, living and working in New York City</span>
                                </div>
                                <div>
                                    @if($club->is_owner(Auth::user()))
                                        <a class="btn btn-light text-blue" href="{{route('club.edit',$club)}}" role="button"><i class="icon-pencil"></i>Edit Club Information</a>
                                    @else
                                        <button class="btn btn-light" data-toggle="button"><i class="icon-add-user"></i> Follow</button>
                                        <button class="btn btn-light"><i class="icon-mail"></i></button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <!--end of section-->
        <section class="flush-with-above space-0">
            <div>
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                @if($club->is_owner(Auth::user()))
                                    <li class="nav-item">
                                        <a class="nav-link @if($tab == 'dashboard') active @endif" href="{{route('club.dashboard',$club)}}" role="tab">Dashboard</a>
                                    </li>
                                @endif
                                <li class="nav-item">
                                    <a class="nav-link @if($tab == 'events') active @endif" href="{{route('club.events',$club)}}" role="tab">Events</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if($tab == 'about') active @endif" href="{{route('club.about',$club)}}" role="tab">About Club</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link @if($tab == 'activities') active @endif" href="{{route('club.activities',$club)}}" role="tab">Activities</a>
                                </li>
                                @if($club->is_owner(Auth::user()))
                                    <li class="nav-item">
                                        <a class="nav-link @if($tab == 'administration') active @endif" href="{{route('club.administration',$club)}}" role="tab">Club Administration</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <!--end of col-->
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </div>
        </section>

        @yield('club-nav-content')

    </div>
@endsection