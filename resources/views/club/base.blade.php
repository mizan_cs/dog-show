@extends('layouts.theme')

@section('content')

    <div class="main-container">
        <section>
            <div class="container">

                <div class="row justify-content-center">
                    <div class="col-12 col-lg-10">
                        @if (session('status'))
                            <div class="alert {{ Session::get('alert-class', 'alert-info') }}" role="alert">
                                {!! session('status') !!}
                            </div>
                        @endif
                        <div class="card card-lg w-100">
                            @if(!empty($club))
                                @if($club->is_owner(Auth::user()))
                                    @if($club->is_publish == 0)
                                        <div class="alert alert-danger text-center" role="alert">
                                            Your club is is not published! <a href="{{route('club.publish',$club)}}">Click Here</a> to publish your club.
                                        </div>
                                    @elseif($club->is_publish == 1 && $club->is_approve == 0)
                                        <div class="alert alert-primary text-center" role="alert">
                                            We are reviewing your club information for approval.
                                        </div>
                                    @endif
                                @endif
                            @endif

                            @yield('club-content')
                        </div>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <!--end of section-->
    </div>

@endsection