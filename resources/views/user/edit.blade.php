@extends('user.base')

@section('dashboard')
    <div class="card card-sm">
        <div class="card-header bg-secondary d-flex justify-content-between align-items-center">
            <div>
                <h6>Edit Profile</h6>
            </div>
        </div>
        <div class="list-group list-group-flush">
            <div class="card">
                <div class="card-body">

                    <form method="post" class="form" action="{{route('user.update',$user)}}">

                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" id="name" value="{{$user->name}}" placeholder="Your Full Name" required>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="address" id="address" value="{{$user->address}}" placeholder="Your Address">
                            @if ($errors->has('address'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="phone" id="phone" value="{{$user->phone}}" placeholder="Your Contact">
                            @if ($errors->has('phone'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="city" id="city" value="{{$user->city}}" placeholder="City">
                            @if ($errors->has('city'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('city') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="city_id_fk" id="city_id_fk" value="{{$user->state_id_fk}}" placeholder="State ID FK">
                            @if ($errors->has('city_id_fk'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('city_id_fk') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="city_zip_code" id="city_zip_code" value="{{$user->zip_code}}" placeholder="Zip Code">
                            @if ($errors->has('city_zip_code'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('city_zip_code') }}</strong>
                                </span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    @if(Auth::user()->is_judge)
        <div class="card card-sm">
            <div class="card-header bg-secondary d-flex justify-content-between align-items-center">
                <div>
                    <h6>Edit Judge Profile</h6>
                </div>
            </div>
            <div class="list-group list-group-flush">
                <div class="card">
                    <div class="card-body">

                        <form method="post" class="form" action="{{route('user.update.judge',$user)}}">

                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="text" class="form-control" name="ukc_judge_number" id="ukc_judge_number" value="{{$user->ukc_judge_number}}" placeholder="UKC Judge's Number">
                                @if ($errors->has('ukc_judge_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ukc_judge_number') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" name="judge_type" id="judge_type" value="{{$user->judge_type}}" placeholder="Judge Type">
                                @if ($errors->has('judge_type'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('judge_type') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" name="judge_likes" id="judge_likes" value="{{$user->judge_likes}}" placeholder="Judge Likes">
                                @if ($errors->has('judge_likes'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('judge_likes') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" name="judge_dislikes" id="judge_dislikes" value="{{$user->judge_dislikes}}" placeholder="Judge Dislikes">
                                @if ($errors->has('judge_dislikes'))
                                    <span class="help-block text-danger">
                                    <strong>{{ $errors->first('judge_dislikes') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" name="judge_rating" id="judge_rating" value="{{$user->judge_rating}}" placeholder="Judge Rating">
                                @if ($errors->has('judge_rating'))
                                    <span class="help-block text-danger">
                                    <strong>{{ $errors->first('judge_rating') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" name="jr_number" id="jr_number" value="{{$user->jr_number}}" placeholder="JR Number">
                                @if ($errors->has('jr_number'))
                                    <span class="help-block text-danger">
                                    <strong>{{ $errors->first('jr_number') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" name="akc_jr_number" id="akc_jr_number" value="{{$user->akc_jr_number}}" placeholder="AKC JR Number">
                                @if ($errors->has('akc_jr_number'))
                                    <span class="help-block text-danger">
                                    <strong>{{ $errors->first('akc_jr_number') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" name="akc_judge_number" id="akc_judge_number" value="{{$user->akc_judge_number}}" placeholder="AKC Judge Number">
                                @if ($errors->has('akc_judge_number'))
                                    <span class="help-block text-danger">
                                    <strong>{{ $errors->first('akc_judge_number') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" name="mr_judge_number" id="mr_judge_number" value="{{$user->mr_judge_number}}" placeholder="MR Judge Number">
                                @if ($errors->has('mr_judge_number'))
                                    <span class="help-block text-danger">
                                    <strong>{{ $errors->first('mr_judge_number') }}</strong>
                                </span>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-primary">Update Judge Information</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection