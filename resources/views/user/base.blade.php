@extends('layouts.theme')

@section('content')

<section class="space-sm">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-9 mb-1 order-md-2">
                @if (session('status'))
                    <div class="alert {{ Session::get('alert-class', 'alert-info') }}" role="alert">
                        {!! session('status') !!}
                    </div>
                @endif
                @yield('dashboard')
            </div>
            <!--end of col-->
            <div class="col-12 col-md-3 mb-1 order-md-1">

                <div class="card card-sm">
                    <a href="{{route('user.view',$user)}}">
                        <div class="card-header">
                            <span class="h6">Dashboard</span>
                        </div>
                    </a>
                    <div class="list-group list-group-flush">
                        <a class="list-group-item d-flex justify-content-between" href="{{route('user.view.dogs',$user)}}">
                            <div>
                                <span>Dogs</span>
                            </div>
                            <div>
                                <i class="icon-chevron-right"></i>
                            </div>
                        </a>

                        @if($user->clubs()->count() != 0 || $user->id == Auth::user()->id)
                            <a class="list-group-item d-flex justify-content-between" href="{{route('user.view.clubs',$user)}}">
                                <div>
                                    <span>Clubs</span>
                                </div>
                                <div>
                                    <i class="icon-chevron-right"></i>
                                </div>
                            </a>
                        @endif

                        @if($user->id == Auth::user()->id)
                            <a class="list-group-item d-flex justify-content-between" href="{{route('user.edit',Auth::user())}}">
                                <div>
                                    <span>Edit Profile</span>
                                </div>
                            </a>
                        @endif


                    </div>
                </div>
                <!-- end card -->
            </div>
            <!--end of col-->
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
@endsection