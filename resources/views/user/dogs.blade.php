@extends('user.base')

@section('dashboard')
    <div class="card card-sm">
        <div class="card-header bg-secondary d-flex justify-content-between align-items-center">
            <div>
                <h6>All Dogs</h6>
            </div>

            @if(Auth::user()->id == $user->id)
                <a class="btn btn-primary" href="{{route('dog.create')}}" role="button">Add New Dogs</a>
            @endif
        </div>
        <div class="list-group list-group-flush">
            <div class="card">
                <div class="card-body">
                    @if($dogs == null)
                        <h6 class="text-center">Empty</h6>
                    @else
                        <div class="col">
                            <ul class="list-group list-group-flush">
                                @foreach($dogs as $dog)
                                    @if($dog->is_publish || $dog->own_by_user() )
                                        <li class="list-group-item">
                                            <div class="media align-items-center">
                                                <a href="#" class="mr-4">
                                                    <img alt="Image" src="{{$dog->image}}" class="rounded avatar avatar-lg">
                                                </a>
                                                <div class="media-body">
                                                    <div class="d-flex justify-content-between mb-2">
                                                        <div>
                                                            <a href="{{route('dog.view',$dog)}}" class="mb-1">
                                                                <h4>{{$dog->call_name}}</h4>
                                                            </a>
                                                            <span>UKC Reg. Name: {{$dog->ukc_registration_name}}</span>
                                                        </div>
                                                        <div class="dropdown">
                                                            <button class="btn btn-sm btn-outline-primary dropdown-toggle dropdown-toggle-no-arrow" type="button" id="SidekickButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="icon-dots-three-horizontal"></i>
                                                            </button>

                                                            <div class="dropdown-menu dropdown-menu-sm" aria-labelledby="SidekickButton">
                                                                @if($dog->own_by_user())
                                                                    <a class="dropdown-item" href="{{route('dog.edit',$dog)}}">Edit</a>
                                                                @endif
                                                                <a class="dropdown-item" href="#">Share</a>
                                                                <a class="dropdown-item" href="#">Comment</a>
                                                                <div class="dropdown-divider"></div>
                                                                <a class="dropdown-item" href="#">Report</a>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    {{--<a class="badge badge-secondary badge-pill mb-2 text-light" >Date of birth: {{$dog->created_at->toFormattedDateString()}}</a>--}}
                                                    <div class="text-small">
                                                        <ul class="list-inline">
                                                            <li class="list-inline-item">Gender: {{$dog->gender}}</li>

                                                        </ul>
                                                        <ul class="list-inline">
                                                            @if(!$dog->is_publish && $dog->own_by_user())
                                                                <li class="list-inline-item"><a class="btn btn-success btn-sm" href="{{route('dog.publish',$dog)}}" role="button">Publish now</a></li>
                                                            @endif

                                                            @if($dog->is_publish && $dog->own_by_user())
                                                                <li class="list-inline-item"><a class="btn btn-danger btn-sm" href="{{route('dog.pause',$dog)}}" role="button">Pause Now</a></li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection