@extends('user.base')

@section('dashboard')
    <div class="card card-sm">
        <div class="card-header bg-secondary d-flex justify-content-between align-items-center">
            <div>
                <h6>Clubs</h6>
            </div>
        </div>
        <div class="list-group list-group-flush">
            <div class="card">
                <div class="card-body">
                    @if($user->clubs()->count() == 0)
                        <h4>Don't Have any Club</h4>
                    @else
                        @foreach($clubs as $club)
                            <div class="card">
                                <div class="card-header card-header-borderless d-flex justify-content-between">
                                    {{--<small class="text-muted"><i class="icon-add-user mr-1"></i> Joined the group <a href="#">Bench</a>--}}
                                    {{--</small>--}}
                                    {{--<small class="text-muted">5 days ago</small>--}}
                                </div>
                                <div class="card-body">
                                    <div class="media">
                                        <a href="#" class="mr-4">
                                            <img alt="Image" src="@if($club->image){{$club->image}}@else {{'https://www.designevo.com/res/templates/thumb_small/red-baseball-club.png'}} @endif" class="avatar-square avatar avatar-lg">
                                        </a>
                                        <div class="media-body">
                                            <div class="d-flex justify-content-between mb-3">
                                                <div>
                                                    <a href="#" class="mb-1">
                                                        <h4>{{$club->name}}</h4>
                                                    </a>
                                                    <span>Designer, living and working in New York City</span>
                                                </div>
                                            </div>

                                            <div class="text-small">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item"><i class="icon-user"></i> 48</li>
                                                    <li class="list-inline-item"><i class="icon-calendar"></i> 9</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer d-flex justify-content-between">

                                    <div class="text-small">
                                        <ul class="list-inline">
                                            @if($club->is_owner(Auth::user()))
                                                <a class="btn btn-sm btn-primary" href="{{route('club.show',$club)}}" role="button">Dashboard</a>
                                            @else
                                                <a class="btn btn-sm btn-primary" href="{{route('club.show',$club)}}" role="button">Show Club</a>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection