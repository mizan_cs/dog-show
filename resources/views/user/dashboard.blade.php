@extends('user.base')

@section('dashboard')
    <div class="card card-sm">
        <div class="card-header bg-secondary d-flex justify-content-between align-items-center">
            <div>
                <h6>Dashboard</h6>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{$user->name}}</h5>
                        <p class="card-text">Address: {{$user->address}}</p>
                        <p class="card-text">City: {{$user->city}}</p>
                    </div>
                </div>
            </div>
            @if($user->is_judge)
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Judge Information</h5>
                            @if($user->ukc_judge_number)<p class="card-text">UKC Judge Number: {{$user->ukc_judge_number}}</p>@endif
                            @if($user->judge_type)<p class="card-text">Judge Type: {{$user->judge_type}}</p>@endif
                            @if($user->judge_rating)<p class="card-text">Judge Rating: {{$user->judge_rating}}</p>@endif
                            @if($user->jr_number)<p class="card-text">JR Number: {{$user->jr_number}}</p>@endif
                            @if($user->akc_jr_number)<p class="card-text">AKC Jr Number: {{$user->akc_jr_number}}</p>@endif
                            @if($user->akc_judge_number)<p class="card-text">AKC Judge Number: {{$user->akc_judge_number}}</p>@endif
                            @if($user->mr_judge_number)<p class="card-text">MR Judge Number: {{$user->mr_judge_number}}</p>@endif
                            @if($user->judge_likes)<p class="card-text">{{$user->judge_likes}} Likes</p>@endif
                            @if($user->judge_dislikes)<p class="card-text">{{$user->judge_dislikes}} Dislikes</p>@endif
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="list-group list-group-flush">
            <a class="list-group-item list-group-item-action" href="#">
                <div class="media">
                    <img alt="Image" src="assets/img/avatar-male-4.jpg" class="avatar">
                    <div class="media-body">
                        <div>
                            <span class="text-muted text-small">Daniel Cameron</span>
                            <h5 class="h6 mb-1">Let's keep those protoypes 100
                                <span class="badge badge-indicator badge-success">&nbsp;</span>
                            </h5>
                            <ul class="list-inline text-small text-muted">
                                <li class="list-inline-item"><i class="icon-code mr-1"></i>Development</li>
                                <li class="list-inline-item"><i class="icon-message mr-1"></i>14</li>
                                <li class="list-inline-item">
                                    <small>Updated: 1 hour ago</small>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection