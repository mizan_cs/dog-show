@extends('layouts.theme')

@section('content')

    <div class="main-container">
        <section>
            <div class="container">

                <div class="row justify-content-center">
                    <div class="col-12 col-lg-10">
                        @if (session('status'))
                            <div class="alert {{ Session::get('alert-class', 'alert-info') }}" role="alert">
                                {!! session('status') !!}
                            </div>
                        @endif
                        <div class="card card-lg w-100">
                            @if(!empty($event))
                                @if($event->is_owner(Auth::user()))
                                    @if($event->is_active == 0)
                                        <div class="alert alert-danger text-center" role="alert">
                                            Your event is not publish yet. <a href="{{route('club.publish',$club)}}">Click Here</a> to publish your event.
                                        </div>
                                    @endif
                                @endif
                            @endif

                            @yield('event-content')
                        </div>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <!--end of section-->
    </div>

@endsection