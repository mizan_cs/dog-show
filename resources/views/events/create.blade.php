@extends('club.show')

@section('club-nav-content')
    <div class="card-body">
        <h6 class="title-decorative text-center">Open New Event</h6>
        <form class="form" method="post" action="{{route('event.store',$club)}}">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">Event Name<code>*</code></label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Club Name" required>
                @if ($errors->has('name'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="start_date">Event Start Date<code>*</code></label>
                <input type="datetime-local" class="form-control" name="start_date" id="start_date" placeholder="Event Start Date" required>
                @if ($errors->has('start_date'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('start_date') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="end_date">Event End Date<code>*</code></label>
                <input type="datetime-local" class="form-control" name="end_date" id="end_date" placeholder="Event End Date" required>
                @if ($errors->has('end_date'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('end_date') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="date_entries_open">Entries Open Date<code>*</code></label>
                <input type="datetime-local" class="form-control" name="date_entries_open" id="date_entries_open" placeholder="Entries Open Date" required>
                @if ($errors->has('date_entries_open'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('date_entries_open') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="date_entries_close">Entries Close Date<code>*</code></label>
                <input type="datetime-local" class="form-control" name="date_entries_close" id="date_entries_close" placeholder="Entries Close Date" required>
                @if ($errors->has('date_entries_close'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('date_entries_close') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="details">Event Descriptions<code>*</code></label>
                <textarea class="form-control" rows="10" name="details" id="details"></textarea>
                @if ($errors->has('details'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('details') }}</strong>
                    </span>
                @endif
            </div>


            <div class="form-group text-center">
                <button class="btn btn-primary" type="submit">Open a new event</button>
            </div>
        </form>
    </div>
@endsection