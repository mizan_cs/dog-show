@extends('layouts.theme')

@section('content')
    <div class="container">

        <img alt="Image" src="{{asset('https://images.wagwalkingweb.com/media/daily_wag/behavior_guides/hero/1537501598.65/why-do-dogs-show-their-teeth.jpg')}}" class="w-100">
        <div class="container">
            <section class="space-sm">
                <div class="container">
                    <div class="row justify-content-between">
                        <div class="col-12 col-md-8 col-lg-7 mt--8">
                            <div class="d-flex align-items-end mb-4">
                                <img alt="Image" src="https://www.designevo.com/res/templates/thumb_small/red-baseball-club.png" class="avatar avatar-lg rounded">
                            </div>
                            <div>
                                <h1 class="mb-0">{{$event->name}}</h1>
                                <h2 class="lead mb-3">{{$event->club->name}}</h2>
                                <div class="d-flex align-items-center">
                                    <span class="badge badge-secondary mr-3">Productivity</span>
                                    <ul class="list text-small d-inline-block">
                                        <li class="list-item"><i class="icon-calendar"></i> Start in {{\Carbon\Carbon::parse($event->start_date)->format('j F Y h:i A')}}</li>
                                        <li class="list-item"><i class="icon-calendar"></i> End at {{\Carbon\Carbon::parse($event->end_date)->format('j F Y h:i A')}}</li>
                                    </ul>
                                </div>
                            </div>
                            <hr>
                            <h5 class="mb-4">Overview</h5>
                            <article>
                                {{$event->details}}
                            </article>
                            <hr>


                        </div>
                        <!--end of col-->
                        <div class="col-12 col-md-4">
                            <a role="button" class="btn btn-success btn-block mb-2" href="">Registeration</a>
                            <div class="card">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6052.255779374734!2d-73.9634766!3d40.671151!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25b0a45bce6bb%3A0x5bf020f45a685ee6!2s200+Eastern+Pkwy%2C+Brooklyn%2C+NY+11238%2C+USA!5e0!3m2!1sen!2sau!4v1512181921736" allowfullscreen="" class="card-img-top w-100"></iframe>
                                <div class="card-body">
                                    <h5>Bench Coworking</h5>
                                    <p>
                                        200 Eastern Pkwy
                                        <br>Brooklyn, NY 11238 USA
                                    </p>
                                    <div>
                                        <a href="#" class="btn btn-outline-primary">Directions</a>
                                        <a href="#" class="btn btn-outline-primary"><i class="icon-phone"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header d-flex justify-content-between">
                                    <div>
                                        <span class="h6">Panel</span>
                                    </div>
                                    <a href="#">View all ›</a>
                                </div>
                                <div class="card-body">
                                    <ul class="list-unstyled list-spacing-sm">
                                        <li>
                                            <a class="media" href="#">
                                                <img alt="Image" src="{{asset('assets/img/avatar-male-1.jpg')}}" class="avatar avatar-sm mr-3">
                                                <div class="media-body">
                                                    <span class="h6 mb-0">Daniel Cameron</span>
                                                    <span class="text-muted">Chairman</span>
                                                </div>
                                            </a>
                                        </li>

                                        <li>
                                            <a class="media" href="#">
                                                <img alt="Image" src="https://wingman.mediumra.re/assets/img/avatar-female-1.jpg" class="avatar avatar-sm mr-3">
                                                <div class="media-body">
                                                    <span class="h6 mb-0">Caitlyn Halsy</span>
                                                    <span class="text-muted">Secretery</span>
                                                </div>
                                            </a>
                                        </li>

                                        <li>
                                            <a class="media" href="#">
                                                <img alt="Image" src="{{asset('assets/img/avatar-male-2.jpg')}}" class="avatar avatar-sm mr-3">
                                                <div class="media-body">
                                                    <span class="h6 mb-0">Toby Marsh</span>
                                                    <span class="text-muted">Chief Steward</span>
                                                </div>
                                            </a>
                                        </li>

                                        <li>
                                            <a class="media" href="#">
                                                <img alt="Image" src="{{asset('assets/img/avatar-female-2.jpg')}}" class="avatar avatar-sm mr-3">
                                                <div class="media-body">
                                                    <span class="h6 mb-0">Dezia Sultana</span>
                                                    <span class="text-muted">Judge</span>
                                                </div>
                                            </a>
                                        </li>

                                        <li>
                                            <a class="media" href="#">
                                                <img alt="Image" src="{{asset('assets/img/avatar-female-2.jpg')}}" class="avatar avatar-sm mr-3">
                                                <div class="media-body">
                                                    <span class="h6 mb-0">Wahiduzzman Herody</span>
                                                    <span class="text-muted">Judge</span>
                                                </div>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="card card-borderless bg-secondary">
                                <a class="card-body" href="#">
                                    <div class="d-flex justify-content-between mb-3">
                                        <img alt="Image" src="{{asset('assets/img/logo-w-gradient.svg')}}">
                                        <span class="title-decorative">Sponsor</span>
                                    </div>
                                    <span class="h6">A robust suite of styled elements and pages for Bootstrap 4</span>
                                </a>
                            </div>
                            <!-- end card -->
                            <div class="card">
                                <div class="card-header d-flex justify-content-between">
                                    <div>
                                        <span class="h6">Photos</span>
                                    </div>
                                    <a href="#">View all ›</a>
                                </div>
                                <div class="card-body">
                                    <div class="image-gallery image-gallery-lg row">
                                        <div class="col">
                                            <img alt="Image" src="{{asset('assets/img/photo-man-laptop.jpg')}}" class="mb-3">
                                            <img alt="Image" src="{{asset('assets/img/photo-man-meeting.jpg')}}">
                                        </div>
                                        <div class="col-6">
                                            <img alt="Image" src="{{asset('assets/img/photo-woman-cafe.jpg')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- end card -->
                            <div class="card">
                                <div class="card-header d-flex justify-content-between">
                                    <div>
                                        <span class="h6">More Events Near You</span>
                                    </div>
                                    <a href="#">View all ›</a>
                                </div>
                                <div class="card-body">
                                    <ul class="list-unstyled list-spacing-sm">

                                        <li>
                                            <div class="media">
                                                <a href="#">
                                                    <img alt="Image" src="{{asset('assets/img/graphic-product-bench-thumb.jpg')}}" class="avatar avatar-square rounded mr-3">
                                                </a>
                                                <div class="media-body">
                                                    <a href="#">
                                                        <span class="h6">Bench</span>
                                                    </a>
                                                    <span class="badge badge-secondary">Productivity</span>
                                                    <div class="text-muted">
                                                        <ul class="list-inline">
                                                            <li class="list-inline-item">
                                                                <small><i class="icon-heart"></i> 373</small>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <small><i class="icon-message"></i> 62</small>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="media">
                                                <a href="#">
                                                    <img alt="Image" src="{{asset('assets/img/graphic-product-kin-thumb.jpg')}}" class="avatar avatar-square rounded mr-3">
                                                </a>
                                                <div class="media-body">
                                                    <a href="#">
                                                        <span class="h6">Kin</span>
                                                    </a>
                                                    <span class="badge badge-secondary">Lifestyle</span>
                                                    <div class="text-muted">
                                                        <ul class="list-inline">
                                                            <li class="list-inline-item">
                                                                <small><i class="icon-heart"></i> 84</small>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <small><i class="icon-message"></i> 21</small>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="media">
                                                <a href="#">
                                                    <img alt="Image" src="{{asset('assets/img/graphic-product-paydar-thumb.jpg')}}" class="avatar avatar-square rounded mr-3">
                                                </a>
                                                <div class="media-body">
                                                    <a href="#">
                                                        <span class="h6">Paydar</span>
                                                    </a>
                                                    <span class="badge badge-secondary">Productivity</span>
                                                    <div class="text-muted">
                                                        <ul class="list-inline">
                                                            <li class="list-inline-item">
                                                                <small><i class="icon-heart"></i> 253</small>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <small><i class="icon-message"></i> 19</small>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="media">
                                                <a href="#">
                                                    <img alt="Image" src="{{asset('assets/img/graphic-product-pipeline-thumb.jpg')}}" class="avatar avatar-square rounded mr-3">
                                                </a>
                                                <div class="media-body">
                                                    <a href="#">
                                                        <span class="h6">pipeline.js</span>
                                                    </a>
                                                    <span class="badge badge-secondary">Development</span>
                                                    <div class="text-muted">
                                                        <ul class="list-inline">
                                                            <li class="list-inline-item">
                                                                <small><i class="icon-heart"></i> 84</small>
                                                            </li>
                                                            <li class="list-inline-item">
                                                                <small><i class="icon-message"></i> 25</small>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <!-- end card -->
                        </div>
                        <!--end of col-->
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
        </div>

        <!--end of section-->

    </div>
@endsection