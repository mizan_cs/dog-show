
<!doctype html>
<html lang="en">

<head>

    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-52115242-13');
    </script>

    <meta charset="utf-8">
    <title>Dogs Show</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,400i,500" rel="stylesheet">
    <link href="{{asset('assets/css/socicon.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('assets/css/entypo.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('assets/css/theme.css')}}" rel="stylesheet" type="text/css" media="all" />
</head>

<body>
@include('sections.nav')
@yield('content')


<script type="text/javascript" src="//code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script type="text/javascript" src="//unpkg.com/smartwizard%404.3.1/dist/js/jquery.smartWizard.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/flickity/2.1.2/flickity.pkgd.min.js"></script>
<script type="text/javascript" src="//unpkg.com/scrollmonitor%401.2.4/scrollMonitor.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/smooth-scroll/12.1.5/js/smooth-scroll.polyfills.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/prism.min.js"></script>
<script type="text/javascript" src="//unpkg.com/zoom-vanilla.js%402.0.6/dist/zoom-vanilla.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{asset('assets/js/theme.js')}}"></script>

</body>



</html>
