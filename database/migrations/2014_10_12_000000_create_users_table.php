<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->text('address')->nullable();
            $table->string('city')->nullable();
            $table->integer('state_id_fk')->nullable();
            $table->string('zip_code')->nullable();
            $table->text('note')->nullable();
            $table->string('attachment')->nullable();
            $table->string('photo')->nullable();
            $table->string('ukc_judge_number')->nullable();
            $table->string('judge_type')->nullable();
            $table->string('judge_likes')->nullable();
            $table->string('judge_dislikes')->nullable();
            $table->string('judge_rating')->nullable();
            $table->string('jr_number')->nullable();
            $table->string('akc_jr_number')->nullable();
            $table->string('akc_judge_number')->nullable();
            $table->string('mr_judge_number')->nullable();
            $table->boolean('is_stwerd')->nullable();
            $table->boolean('is_judge')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
