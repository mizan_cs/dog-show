<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdministrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('club_id')->unsigned();
            $table->foreign('club_id')->references('id')->on('clubs');
            $table->string('Role')->nullable();
            $table->boolean('can_edit_event')->default(false);
            $table->boolean('can_edit_disable')->default(false);
            $table->boolean('can_add_event')->default(false);
            $table->boolean('can_add_user')->default(false);
            $table->boolean('can_add_see_dashboard')->default(false);
            $table->boolean('can_add_notice')->default(false);
            $table->boolean('can_send_message')->default(false);
            $table->boolean('can_read_message')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrations');
    }
}
