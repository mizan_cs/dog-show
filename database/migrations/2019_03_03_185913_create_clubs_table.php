<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('club_number')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('phone')->nullable();
            $table->text('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state_id_fk')->nullable();
            $table->string('zip_code')->nullable();
            $table->text('note')->nullable();
            $table->string('attachment')->nullable();
            $table->string('form_club_logo')->nullable();
            $table->string('report_club_logo')->nullable();
            $table->string('home_club_logo')->nullable();
            $table->string('akc_club_name')->nullable();
            $table->string('akc_club_number')->nullable();
            $table->string('akc_form_club_logo')->nullable();
            $table->string('akc_report_club_logo')->nullable();
            $table->boolean('is_publish')->default(false);
            $table->boolean('is_approve')->default(false);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clubs');
    }
}
