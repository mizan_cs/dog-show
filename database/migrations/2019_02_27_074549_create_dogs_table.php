<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ukc_registration_number')->nullable();
            $table->string('call_name')->nullable();
            $table->string('ukc_registration_name')->nullable();
            $table->string('ukc_breed')->nullable();
            $table->string('variety')->nullable();
            $table->string('gender')->nullable();
            $table->text('notes')->nullable();
            $table->string('attachment')->nullable();
            $table->string('search_cue')->nullable();
            $table->string('final_response')->nullable();
            $table->string('image')->nullable();
            $table->string('ukc_permanent')->nullable();
            $table->string('ukctl')->nullable();
            $table->string('ukclp')->nullable();
            $table->integer('height')->nullable();
            $table->boolean('veteran')->nullable();
            $table->string('akc_registration_number')->nullable();
            $table->string('akc_registration_type')->nullable();
            $table->string('akc_foriegn')->nullable();
            $table->string('akc_breed_id_fk')->nullable();
            $table->string('breeder')->nullable();
            $table->string('sire')->nullable();
            $table->string('dam')->nullable();
            $table->boolean('is_publish')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dogs');
    }
}
